defmodule Blaster.HTTP.Connection do

  @moduledoc """
  Functions for creating and interacting with HTTP connections.
  Gun Library stuff goes in here too.
  """

  defstruct(
    path: "/",
    port: 80,
    host: 'example.com',
    stream_ref: nil,
    gun_pid: nil,
    monitor_ref: nil,
    from: nil,
    response: %{ body: "", headers: [], status_code: 0 }
  )

  def new_connection(attrs) do
    attrs
    |> Enum.into(%{})
    |> handle_host_strings()
    |> create_struct()
  end

  def connect(%{ host: host, port: port } = client) do
    {:ok, _} = :application.ensure_all_started(:gun)

    connect_opts =
      ssl_options(host, port)
      |> Enum.into(%{
        connect_timeout: :timer.minutes(1),
        retry: 10,
        retry_timeout: 100,
        http_opts: %{keepalive: :infinity},
        http2_opts: %{keepalive: 5000}
      })

    case open_connection(host, port, connect_opts) do
      { :ok, gun_pid } ->
        %{ client | gun_pid: gun_pid }
      { :error, _ } ->
        client
    end
  end

  def request(client = %{ gun_pid: pid }, opts, from) do
    %{ client | monitor_ref: Process.monitor(pid), from: from }
    |> do_request(opts)
  end

  def add_response(client, response) do
    %{ client | response: response }
  end

  def add_response_data(client, data) do
    client
    |> Map.from_struct()
    |> update_in([:response, :body], & &1 <> data)
    |> create_struct()
  end

  def flush_response(client) do
    Map.get_and_update(client, :response, fn response -> { response, %{} } end)
  end

  defp do_request(%{ gun_pid: nil } = http, _), do: http

  defp do_request(%{ gun_pid: gun_pid } = http, %{ type: :get } = opts) do
    %{ query: query, headers: headers } = opts

    %{ http | stream_ref: :gun.get(gun_pid, to_charlist(query), convert_to_list(headers)) }
  end

  defp do_request(%{ gun_pid: gun_pid } = http, %{ type: :post } = opts) do
    %{ query: query, headers: headers, body: body } = opts
    headers =
      headers
      |> Enum.into(%{ "content-length" => byte_size(body) })
      |> convert_to_list()

    %{ http | stream_ref: :gun.post(gun_pid, to_charlist(query), headers, body) }
  end

  defp do_request(%{ gun_pid: gun_pid } = http, %{ type: :delete } = opts) do
    %{ query: query, headers: headers } = opts

    %{ http | stream_ref: :gun.delete(gun_pid, to_charlist(query), convert_to_list(headers)) }
  end

  defp ssl_options(host, 443) do
    %{
      transport: :tls,
      tls_opts: [
        verify: :verify_peer,
        cacerts: :certifi.cacerts(),
        depth: 99,
        server_name_indication: host,
        reuse_sessions: false,
        verify_fun: {&:ssl_verify_hostname.verify_fun/3, [check_hostname: host]}
      ]
    }
  end

  defp ssl_options(_host, _no_ssl), do: %{}

  defp open_connection(host, port, connect_opts) do
    with {:ok, conn_pid} <- :gun.open(host, port, connect_opts),
         {:ok, _protocol} <- :gun.await_up(conn_pid, :timer.minutes(1)) do
      {:ok, conn_pid}
    else
      {:error, reason} ->
        {:error, reason}
    end
  end

  defp handle_host_strings(args = %{ host: host }) when is_binary(host) do
    host =
      host
      |> remove_protocol()
      |> to_charlist()
    %{ args | host: host }
  end

  defp handle_host_strings(args), do: args

  defp remove_protocol(host) do
    String.replace(host, ~r"^(ht|f)tp(s?)://", "")
  end

  defp create_struct(attrs) do
    struct(__MODULE__, attrs)
  end

  defp convert_to_list(headers) do
    Enum.map(headers, fn { name, value } -> { name, to_charlist(value) } end)
  end
end

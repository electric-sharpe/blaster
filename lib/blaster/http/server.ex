defmodule Blaster.HTTP.Server do

  @moduledoc """
  GenServer which handles an HTTP connection. It restarts if it shuts down.
  This is a demonstration of how to use gun with an GenServer wrapper in Elixir
  """

  use GenServer

  alias Blaster.HTTP.Connection

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts)
  end

  def init(opts) do
    {:ok, Connection.new_connection(opts) }
  end

  def handle_call({:open_http}, _from, conn) do
    {:reply, :ok, Connection.connect(conn) }
  end

  def handle_call({:request, details}, from, conn) do
    {:noreply, Connection.request(conn, details, from)}
  end

  # Gun Messages

  @doc """
  Handle gun response messages
  """
  def handle_info({ :gun_response, pid, ref, fin, status, headers }, %{ gun_pid: pid, stream_ref: ref } = state) do
    state =
      state
      |> Connection.add_response(%{ status_code: status, headers: headers, body: "" })
      |> handle_data(fin)

    { :noreply, state }
  end

  @doc """
  Handle gun data messages
  """
  def handle_info({ :gun_data, pid, ref, fin, data }, %{ gun_pid: pid, stream_ref: ref } = state) do
    state =
      state
      |> Connection.add_response_data(data)
      |> handle_data(fin)

    { :noreply, state }
  end

  @doc """
  Handle error messages
  """

  def handle_info({ :gun_error, pid, ref, reason }, %{ gun_pid: pid, stream_ref: ref } = state) do
    send_error_message(state, reason)
    { :noreply, state }
  end

  def handle_info({ :gun_error, pid, error }, %{ gun_pid: pid } = state) do
    send_error_message(state, error)
    { :noreply, state }
  end

  def handle_info({ :gun_down, pid, _, reason, _ }, %{ gun_pid: pid } = state) do
    send_error_message(state, reason)
    { :noreply, state }
  end

  def handle_info({ :DOWN, mon, :process, pid, reason }, %{ gun_pid: pid, monitor_ref: mon } = state) do
    send_error_message(state, reason)
    { :noreply, state }
  end

  @doc """
  If the gun_pid matches, restart
  """
  def handle_info({:gun_down, gun_pid, _http_ws, :closed, []}, %{gun_pid: gun_pid} = state) do
    {:noreply, Connection.connect(state) }
  end

  @doc """
  If normal exit, restart
  """
  def handle_info({:gun_down, gun_pid, _http_ws, :normal, _stream_refs}, %{gun_pid: gun_pid} = state) do
    {:noreply, Connection.connect(state) }
  end

  @doc """
  If the gun_pid doesn't match, don't restart, it's probably obsolete.
  """
  def handle_info({:gun_down, _alt_gun_pid, _http_ws, :closed, []}, %{gun_pid: _gun_pid} = state) do
    {:noreply, state}
  end

  @doc """
  If normal exit, but different pid, don't restart
  """
  def handle_info({:gun_down, _alt_gun_pid, _http_ws, :normal, []}, %{gun_pid: _gun_pid} = state) do
    {:noreply, state }
  end

  @doc """
  Handle gun_up - this can be monitored if desired.
  """
  def handle_info({:gun_up, gun_pid, _http_ws}, %{gun_pid: gun_pid} = state) do
    {:noreply, state}
  end

  @doc """
  Handle gun_up - this can be monitored if desired.
  """
  def handle_info({:gun_up, _alt_gun_pid, _http_ws}, %{gun_pid: _gun_pid} = state) do
    {:noreply, state}
  end

  defp send_error_message(%{ from: nil }, message), do: message

  defp send_error_message(%{ from: from }, message) do
    GenServer.reply(from, %{ message: message })
  end

  defp handle_data(state, :fin) do
    { response, state } = Connection.flush_response(state)
    GenServer.reply(state.from, response)
    state
  end

  defp handle_data(state, :nofin), do: state
end

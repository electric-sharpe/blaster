defmodule Blaster.WebSocket.Connection do

  @moduledoc """
  Functions for creating and interacting with WebSocket connections.
  Gun Library stuff goes in here too.
  """

  defstruct(
    path: "/",
    port: 80,
    host: 'echo.websocket.org',
    stream_ref: nil,
    gun_pid: nil,
    from: nil
  )

  def new_connection(attrs) do
    attrs
    |> Enum.into(%{})
    |> handle_host_strings()
    |> create_struct()
  end

  def connect(ws), do: ws_upgrade(ws)
  def reconnect(ws), do: ws_upgrade(ws)

  def send_message(%{ gun_pid: pid }, message) do
    :gun.ws_send(pid, {:text, message})
  end

  defp ws_upgrade(%{ path: path, port: port, host: host } = state) do
    {:ok, _} = :application.ensure_all_started(:gun)

    connect_opts =
      ssl_options(state)
      |> Enum.into(%{
        connect_timeout: :timer.minutes(1),
        retry: 10,
        retry_timeout: 300
      })

    case open_connection(host, port, connect_opts) do
      {:ok, gun_pid} ->
        stream_ref = :gun.ws_upgrade(gun_pid, path)
        %{state | stream_ref: stream_ref, gun_pid: gun_pid}
      {:error, _ } ->
        state
    end
  end

  defp open_connection(host, port, connect_opts) do
    with {:ok, conn_pid} <- :gun.open(host, port, connect_opts),
         {:ok, _protocol} <- :gun.await_up(conn_pid, :timer.minutes(1)) do
      {:ok, conn_pid}
    else
      {:error, reason} ->
        {:error, reason}
    end
  end

  defp handle_host_strings(args = %{ host: host }) when is_binary(host) do
    host =
      host
      |> remove_protocol()
      |> to_charlist()
    %{ args | host: host }
  end

  defp handle_host_strings(args), do: args

  defp remove_protocol(host) do
    String.replace(host, ~r"^ws(s?)://", "")
  end

  defp create_struct(attrs), do: struct(__MODULE__, attrs)

  # When trying to connect to WSS, restrict the protocol to
  # HTTP/1.1, per https://ninenines.eu/docs/en/gun/1.3/guide/websocket/
  defp ssl_options(%{ port: 443 }) do
    %{ protocols: [:http] }
  end
  defp ssl_options(_), do: %{}
end

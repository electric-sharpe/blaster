defmodule Blaster.WebSocket.Server do

  @moduledoc """
  GenServer which handles a websocket connection. It restarts if it shuts down.
  This is a demonstration of how to use gun with an GenServer wrapper in Elixir
  """

  use GenServer, restart: :transient

  alias Blaster.WebSocket.Connection

  require Logger

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts)
  end

  def init(opts) do
    {:ok, Connection.new_connection(opts) }
  end

  @doc """
  This sends back the current state of this server
  """
  def handle_call({:get_state}, _from, state) do
    {:reply, state, state}
  end

  @doc """
  This opens the websocket which includes
  get > upgrade > success
  This sets the relevant values in state
  This is blocking so the client can't send until the websocket is open
  This also sets from so you can send the results back to the calling process
  """
  def handle_call({:open_ws}, from, state) do
    {:reply, :ok, Connection.connect(%{ state | from: from }) }
  end

  @doc """
  This sends a message after the websocket connection has been opened
  """
  def handle_cast({:send_message, message}, state) do
    :ok = Connection.send_message(state, message)
    {:noreply, state}
  end

  ####
  # gun upgrade functions
  ####

  @doc """
  This is the one it hits if upgrade is successful and from is nil (not set)
  """
  def handle_info({:gun_upgrade, gun_pid, stream_ref, ["websocket"], headers},
    %{stream_ref: stream_ref, gun_pid: gun_pid, from: nil} = state
    ) do
    Logger.debug("Upgraded #{inspect(gun_pid)}. Success!\nHeaders:\n#{inspect(headers)}")
    {:noreply, state}
  end

  @doc """
  This is the one it hits if upgrade is successful and from is set
  This sends a message back to the calling function that the websocket is open and can send and receive messages
  """
  def handle_info({:gun_upgrade, gun_pid, stream_ref, ["websocket"], _headers},
    %{stream_ref: stream_ref, gun_pid: gun_pid, from: from} = state
    ) do
    # Give from process a message that the websocket is now open.
    GenServer.reply(from, {:websocket_open})
    {:noreply, state}
  end

  @doc """
  Upgrade not successful
  """
  def handle_info({:gun_response, _gun_pid, _, _, status, headers}, _) do
    Logger.error "Websocket upgrade failed."
    exit({:ws_upgrade_failed, status, headers})
  end

  @doc """
  Error on upgrade
  """
  def handle_info({:gun_error, _gun_pid, _stream_ref, reason}, _) do
    exit({:ws_upgrade_failed, reason})
  end


  ####
  # gun messages
  ####

  @doc """
  Receiving message with type :text or :binary
  """
  def handle_info({:gun_ws, gun_pid, stream_ref, {message_type, message}}, %{stream_ref: stream_ref, gun_pid: gun_pid} = state)
  when message_type in [:text, :binary] do
    handle_message(state, message)
    {:noreply, state}
  end

  @doc """
  If the gun_pid matches, restart
  """
  def handle_info({:gun_down, gun_pid, _http_ws, :closed, [], []}, %{gun_pid: gun_pid} = state) do
    {:noreply, Connection.reconnect(state) }
  end

  @doc """
  If the gun_pid doesn't match, don't restart, it's probably obsolete.
  """
  def handle_info({:gun_down, _alt_gun_pid, _http_ws, :closed, [], []}, %{gun_pid: _gun_pid} = state) do
    {:noreply, state}
  end

  @doc """
  This is instructing gun to close, but with no code. That means the websocket is closed.
  """
  def handle_info({:gun_ws, gun_pid, stream_ref, :close}, %{stream_ref: stream_ref, gun_pid: gun_pid} = state) do
    { :stop, :normal, state }
  end

  @doc """
  This is instructing gun to close, so we restart
  This could be more granular by matching on the code which is currently ignored.
  """
  def handle_info({:gun_ws, gun_pid, stream_ref, {:close, _code, ""}}, %{stream_ref: stream_ref, gun_pid: gun_pid} = state) do
    # Re-open the websocket connection if it closes here.
    {:noreply, Connection.reconnect(state) }
  end

  @doc """
  Handle gun_up - this can be monitored if desired.
  http_ws is :http or :ws
  """
  def handle_info({:gun_up, gun_pid, _http_ws}, %{gun_pid: gun_pid} = state) do
    {:noreply, state}
  end

  @doc """
  Handle gun_up - this can be monitored if desired.
  http_ws is :http or :ws
  """
  def handle_info({:gun_up, _alt_gun_pid, _http_ws}, %{gun_pid: _gun_pid} = state) do
    {:noreply, state}
  end

  @doc """
  If this hits, something unexpected happened. Perhaps an error.
  This is meant to be a catch all for all other messages.
  """
  def handle_info(message, state) do
    Logger.warn "Unexpected message: #{inspect message, pretty: true} with state: #{inspect state, pretty: true}"
    {:noreply, state}
  end

  # No calling process: just log the message
  defp handle_message(%{ from: nil }, message) do
    Logger.debug("Message received #{inspect message}")
  end

  # Calling process: send the message along
  defp handle_message(%{ from: from }, message) do
    GenServer.reply(from, %{message: message})
  end
end

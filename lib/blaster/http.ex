defmodule Blaster.HTTP do

  @moduledoc """
  Functions for making HTTP requests.
  """

  alias Blaster.HTTP.Server

  require Logger

  @success 200..299
  @client_errors 400..499
  @server_errors 500..599

  @doc """
  Create a new request. Returns the PID of the client.
  """
  def new_client(opts) do
    { :ok, client_pid } = Server.start_link(opts)
    client_pid
  end

  @doc """
  Connect to an HTTP endpoint. Required before making requests
  to it.
  """
  def connect(client_pid) do
    GenServer.call(client_pid, {:open_http})
  end

  @doc """
  Make a GET request to an HTTP endpoint.
  """
  def get(client_pid, query, headers) do
    GenServer.call(client_pid, {
      :request,
      %{ type: :get, query: query, headers: headers }
    })
    |> handle_response()
  end

  @doc """
  Make a POST request to an HTTP endpoint.
  """
  def post(client_pid, query, body, headers) do
    GenServer.call(client_pid, {
      :request,
      %{ type: :post, query: query, headers: headers, body: body }
    })
    |> handle_response()
  end

  def delete(client_pid, query, headers) do
    GenServer.call(client_pid, {
      :request,
      %{ type: :delete, query: query, headers: headers }
    })
    |> handle_response()
  end

  defp handle_response(%{ status_code: code, body: body }) when code in @success do
    Jason.decode!(body)
  end

  defp handle_response(%{ status_code: code, body: body }) when code in @client_errors do
    Logger.warn("HTTP #{code}: #{inspect body}")
    { :error, error_message(code) }
  end

  defp handle_response(%{ status_code: code, body: body }) when code in @server_errors do
    Logger.error("HTTP #{code}: #{inspect body}")
    { :error, code }
  end

  defp handle_response(%{ status_code: code }) do
    { :error, code }
  end

  # Handle gun-related errors
  defp handle_response(%{ message: error = { :error, _ } }) do
    error
  end

  defp handle_response(%{ message: error }) do
    { :error, error }
  end

  defp error_message(400), do: :bad_request
  defp error_message(403), do: :forbidden
  defp error_message(404), do: :not_found
  defp error_message(code), do: code
end

defmodule Blaster.WebSocket do

  alias Blaster.WebSocket.Server
  @doc """
  Creates a new socket
  """
  def new_socket(opts) do
    { :ok, socket_pid } = Server.start_link(opts)
    socket_pid
  end

  @doc """
  Connect to the websocket endpoint
  """
  def connect(socket_pid) do
    GenServer.call(socket_pid, {:open_ws})
  end

  @doc """
  Send a message to the websocket endpoint
  """
  def send_message(socket_pid, message) do
    GenServer.cast(socket_pid, {:send_message, message})
  end

  @doc """
  Send a synchronous message to the websocket
  """
  def send_sync(socket_pid, args_tuple) do
    GenServer.call(socket_pid, args_tuple)
  end

  @doc """
  Send an asynchronous message to the websocket
  """
  def send_async(socket_pid, args_tuple) do
    GenServer.cast(socket_pid, args_tuple)
  end
end

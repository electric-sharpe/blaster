defmodule Blaster do
  @moduledoc """
  Public Blaster interface
  """

  alias Blaster.{WebSocket, HTTP}

  @doc """
  Creates a new WebSocket
  """
  defdelegate new_socket(opts \\ []), to: WebSocket

  @doc """
  Connects to a WebSocket
  """
  def ws_connect(socket) do
    :ok = WebSocket.connect(socket)
    socket
  end

  @doc """
  Creates a new HTTP client
  """
  defdelegate new_client(opts \\ []), to: HTTP

  @doc """
  Connects to an HTTP client
  """
  def http_connect(client) do
    :ok = HTTP.connect(client)
    client
  end


  @doc """
  Creates, then connects to a socket or client.
  """
  def connect(opts \\ []) do
    { type, opts } = Keyword.pop(opts, :type, :ws)
    connect(opts, type)
  end

  @doc """
  Creates, then connects to a WebSocket
  """
  def connect(opts, :ws) do
    opts
    |> new_socket()
    |> ws_connect()
  end

  @doc """
  Creates, then connects to an HTTP client
  """
  def connect(opts, :http) do
    opts
    |> new_client()
    |> http_connect()
  end

  @doc """
  Sends a message to the WebSocket
  """
  defdelegate send_message(socket, message), to: WebSocket

  @doc """
  Perform a GET request
  """
  defdelegate get(client, query, headers \\ %{}), to: HTTP

  @doc """
  Perform a POST request
  """
  defdelegate post(client, query, body, headers \\ %{}), to: HTTP

  @doc """
  Perform a DELETE request
  """
  defdelegate delete(client, query, headers \\ %{}), to: HTTP
end

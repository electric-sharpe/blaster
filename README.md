# Blaster

Yet another HTTP/WebSocket connection library based on Gun. Not intended for
general use. Please don't use it in your projects.


## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `blaster` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:blaster, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/blaster](https://hexdocs.pm/blaster).


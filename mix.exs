defmodule Blaster.MixProject do
  use Mix.Project

  def project do
    [
      app: :blaster,
      version: "0.1.4",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:gun, github: "ninenines/gun"},
      {:jason, "~> 1.0"},
      {:cowlib, "~> 2.8.0", override: true},
      {:ssl_verify_hostname, "~> 1.0"},
      {:certifi, "~> 2.5"},
    ]
  end
end
